{
  # The nixpkgs instance to use
  pkgs ? import <nixpkgs> { }
  # Extra inputs to use
, args ? { }
  # Global packages to always be installed
, globals ? [ pkgs.coreutils ]
  # If set, this will only prepend to path rather than set it
, impure ? false
} @ attrs:
let
  inherit (pkgs) lib;

  # Default purepkgs modules
  defaultModules = [
    {
      _module.args = {
        inherit pkgs;
        _purepkgs = attrs;
      } // args;
    }
    ./modules
  ];

  # Evaluate a package from modules
  evalPackage = modules:
    let
      evaluated = pkgs.lib.evalModules {
        modules = defaultModules ++ (lib.toList modules);
      };
    in
    evaluated.config.program;

  self = {
    # Create a package from modules
    mkPackage = modules:
      let
        evaluated = evalPackage modules;
        pkg = evaluated.output;
        mainPath = evaluated.mainBinary;
      in
      pkg // {
        # Add one or more new modules to be considered
        configure = newModules: self.mkPackage (lib.toList modules ++ lib.toList newModules);

        # Evaluate package
        package = pkg;

        # Main binary of package
        main = "${pkg}/${mainPath}";
      };

    # Default configurable packages
    packages = import ./programs {
      inherit pkgs;
      purepkgs = self;
    };

    # Return purepkgs with updated settings
    override = x: (
      if builtins.typeOf x == "lambda" then
        import ./. (attrs // (x attrs))
      else if builtins.typeOf x == "set" then
        import ./. (attrs // x)
      else
        throw "Don't know how to override with ${builtins.typeOf x}"
    );

    # Import module with purepkgs as argument
    callPackage = file: args: import file (
      {
        purepkgs = self;
        inherit pkgs;
      } // args
    );
  };
in
self
