{ pkgs, purepkgs, }:

{
  less = purepkgs.mkPackage ./less;
  neovim = purepkgs.mkPackage ./neovim;
  zsh = purepkgs.mkPackage ./zsh;
}
