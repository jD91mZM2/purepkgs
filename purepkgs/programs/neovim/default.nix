{ pkgs, lib, config, ... }:

with lib;

let
  cfg = config;
in
{
  options = {
    neovim = mkOption {
      type = types.package;
      description = "NeoVim source derivation to be configured";
      default = pkgs.neovim;
    };
    customRC = mkOption {
      type = types.lines;
      description = "Your NeoVim's init.vim";
      default = "";
      example = literalExample ''
        "source ''${initDir}/init.vim"
      '';
    };
    packages = mkOption {
      type = types.attrsOf (types.submodule ({
        options = {
          start = mkOption {
            type = types.listOf types.package;
            description = "Plugins to be immediately loaded when vim starts";
            default = [ ];
          };
          opt = mkOption {
            type = types.listOf types.package;
            description = "Plugins to be lazily loaded with :packadd";
            default = [ ];
          };
        };
      }));
      description = "Vim packages to be installed";
      default = { };
      example = literalExample ''
        {
          nixPackages = with pkgs.vimPlugins; {
            # This package is loaded immediately when vim starts
            start = [
              fugitive
            ];

            # This package is loaded with :packadd vim-nix
            opt = [
              vim-nix
            ]
          };
        }
      '';
    };
  };

  config.program = {
    drv = cfg.neovim.override {
      configure = {
        inherit (cfg) customRC;
      } // (optionalAttrs (cfg.packages != { }) {
        inherit (cfg) packages;
      });
    };

    mainBinary = "bin/nvim";
  };
}
