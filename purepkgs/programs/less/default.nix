{ pkgs, lib, config, ... }:

with lib;
let
  cfg = config;

  type = types.listOf (types.listOf types.str);
  suboption = {
    begin = mkOption {
      inherit type;
      description = "tput commands to activate";
      default = [ ];
    };
    end = mkOption {
      inherit type;
      description = "tput commands to deactivate";
      default = [ ];
    };
  };

  tput = termcaps:
    if termcaps == [ ] then
      ""
    else
      "$(tput "
      + (concatStringsSep ")$(tput " (map escapeShellArgs termcaps))
      + ")";
in
{
  options = {
    colours = {
      bold = suboption;
      underline = suboption;
      standout = suboption;
    };
  };

  config.program = {
    drv = pkgs.less;
    runs = ''
      export LESS_TERMCAP_md="${tput cfg.colours.bold.begin}"
      export LESS_TERMCAP_me="${tput cfg.colours.bold.end}"
      export LESS_TERMCAP_us="${tput cfg.colours.underline.begin}"
      export LESS_TERMCAP_ue="${tput cfg.colours.underline.end}"
      export LESS_TERMCAP_so="${tput cfg.colours.standout.begin}"
      export LESS_TERMCAP_se="${tput cfg.colours.standout.end}"
    '';
  };
}
