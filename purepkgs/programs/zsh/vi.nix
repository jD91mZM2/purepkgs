{ config, lib, ... }:

with lib;
let
  cfg = config.vi;
in
{
  options.vi = {
    enable = mkEnableOption "Enable vi bindings in ZSH";
  };
  config = mkIf cfg.enable {
    interactiveShellInit = mkAfter ''
      # Vi keybindings
      bindkey -v
      export KEYTIMEOUT=1

      # Sync with cursor
      vi-mode-cursor() {
        case $KEYMAP in
          vicmd)
            echo -ne "\e[2 q"
            ;;
          viins|main)
            echo -ne "\e[6 q"
            ;;
        esac
      }
      zle -N zle-keymap-select vi-mode-cursor
      zle -N zle-line-init vi-mode-cursor
    '';
  };
}
