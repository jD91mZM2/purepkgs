{ pkgs, lib, config, ... }:

with lib;
let
  cfg = config;
in
{
  imports = [
    ./plugins.nix
    ./vi.nix
  ];

  options = {
    shellInit = mkOption {
      type = types.lines;
      description = "Your .zshenv";
      default = "";
    };
    interactiveShellInit = mkOption {
      type = types.lines;
      description = "Your .zshrc";
      default = "";
    };
    loginShellInit = mkOption {
      type = types.lines;
      description = "Your .zprofile";
      default = "";
    };
    prompt = mkOption {
      type = types.lines;
      description = "Your shell prompt";
      default = "";
    };
    aliases = mkOption {
      type = types.attrsOf types.str;
      description = "Aliases";
      default = { };
    };
  };

  config =
    let
      write = name: text: pkgs.writeTextFile {
        inherit name text;
      };
    in
    mkMerge [
      (mkIf (cfg.prompt != "") {
        interactiveShellInit = ''
          promptFunc() {
            ${cfg.prompt}
          }
          promptInit() {
            PS1="$(promptFunc)"
          }
          precmd_functions+=promptInit
        '';
      })
      (mkIf (cfg.aliases != { }) {
        interactiveShellInit = concatStringsSep "\n" (
          mapAttrsToList
            (name: value: "alias ${escapeShellArg name}=${escapeShellArg value}")
            cfg.aliases
        );
      })
      {
        program = {
          drv = pkgs.zsh;

          passthru = {
            shellPath = "/" + cfg.program.mainBinary;
          };

          # Preserve purity by disabling global configs (unfortunately excludes /etc/zshenv)
          args = "+o GLOBAL_RCS";

          # ZSH wants to write to $ZDOTDIR, so we'll let it keep all the state
          # (history, etc) under ~/.cache
          runs = ''
            export ZDOTDIR="''${XDG_CACHE_DIR:-$HOME/.cache}/zsh"
            mkdir -p "$ZDOTDIR"

            # Let zsh load history from this cache directory
            export HISTFILE="$ZDOTDIR/.zsh_history"

            install -m 644 "${write "zshenv" cfg.shellInit}" "$ZDOTDIR/.zshenv"
            install -m 644 "${write "zshrc" cfg.interactiveShellInit}" "$ZDOTDIR/.zshrc"
            install -m 644 "${write "zprofile" cfg.loginShellInit}" "$ZDOTDIR/.zprofile"
          '';
        };
      }
    ];
}
