{ pkgs, lib, config, ... }:

with lib;
let
  cfg = config;
in
{
  options = {
    autosuggestions = {
      enable = mkEnableOption "Zsh autosuggestions plugin";
      highlightStyle = mkOption {
        type = types.str;
        description = "Highlight style";
        example = "bg=#000000,fg=#FFFFFF";
        default = "";
      };
    };
    syntaxHighlighting.enable = mkEnableOption "Zsh syntax highlighting plugin";
  };
  config = mkMerge [
    (mkIf cfg.autosuggestions.enable {
      interactiveShellInit = mkBefore ''
        source "${pkgs.zsh-autosuggestions}/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
        export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE=${escapeShellArg cfg.autosuggestions.highlightStyle}
      '';
    })
    (mkIf cfg.syntaxHighlighting.enable {
      interactiveShellInit = mkBefore ''
        source "${pkgs.zsh-syntax-highlighting}/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
      '';
    })
  ];
}
