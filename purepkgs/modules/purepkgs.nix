{ pkgs, lib, config, _purepkgs, ... }:

with lib;
let
  cfg = config.purepkgs;
in
{
  options.purepkgs = {
    globals = mkOption {
      type = types.listOf types.package;
      description = "The user's global packages";
      default = _purepkgs.globals or [ pkgs.coreutils ];
      readOnly = true;
    };
    impure = mkOption {
      type = types.bool;
      description = "If set, this program can access the user's existing PATH";
      default = _purepkgs.impure or false;
      readOnly = true;
    };
  };

  config.program = {
    runtimeDeps = cfg.globals;
  };
}
