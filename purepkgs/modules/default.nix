{ pkgs, lib, config, ... }:

with lib;
let
  cfg = config.program;
in
{
  imports = [
    ./purepkgs.nix
  ];

  options.program = {
    drv = mkOption {
      type = types.package;
      description = "Main derivation";
    };
    mainBinary = mkOption {
      type = types.str;
      description = "Relative path of the main binary of derivation";
      defaultText = "bin/\${drv.pname or drv.name}";
    };
    extraDrvs = mkOption {
      type = types.listOf types.package;
      description = "Extra derivations to be merged";
      default = [ ];
    };
    installScript = mkOption {
      type = types.lines;
      description = "Additional code in the installation script";
      default = "";
    };

    runtimeDeps = mkOption {
      type = types.listOf types.package;
      description = "Runtime dependencies";
      default = [ ];
    };
    env = mkOption {
      type = types.attrsOf types.str;
      description = "Environment variables";
      default = { };
    };
    args = mkOption {
      type = types.separatedString " ";
      description = "Program arguments";
      default = "";
    };
    runs = mkOption {
      type = types.lines;
      description = "Wrapper code that runs before all executables";
      default = "";
    };
    makeWrapperArgs = mkOption {
      type = types.listOf types.str;
      description = "Raw makeWrapper arguments";
      default = [ ];
    };
    passthru = mkOption {
      type = types.attrs;
      description = "Varibles to pass through derivation";
      default = { };
    };

    output = mkOption {
      type = types.package;
      description = "Output derivation. Internal variable.";
      visible = false;
    };
  };

  config.program =
    let
      pname = cfg.drv.pname   or (builtins.parseDrvName cfg.drv.name).name;
      version = cfg.drv.version or (builtins.parseDrvName cfg.drv.name).version;
    in
    mkMerge [
      # Add environment variables to makeWrapperArgs
      {
        makeWrapperArgs = concatMap (list: list) (
          mapAttrsToList
            (key: value: [ "--set" key value ])
            cfg.env
        );
      }

      # Add program arguments to makeWrapperArgs
      (mkIf (cfg.args != "") {
        makeWrapperArgs = [ "--add-flags" cfg.args ];
      })

      # Add snippets to run
      (mkIf (cfg.runs != "") {
        makeWrapperArgs = [ "--run" cfg.runs ];
      })

      # Add runtime dependencies to makeWrapperArgs
      {
        makeWrapperArgs =
          if config.purepkgs.impure then
            [ "--prefix" "PATH" ":" (makeBinPath cfg.runtimeDeps) ]
          else
            [ "--set" "PATH" (makeBinPath cfg.runtimeDeps) ];
      }

      {
        mainBinary = mkOverride 500 "bin/${pname}";

        output = pkgs.symlinkJoin {
          inherit pname version;
          name = "${pname}-${version}";

          paths = [ cfg.drv ] ++ cfg.extraDrvs;

          passthru = cfg.passthru;

          nativeBuildInputs = [ pkgs.makeWrapper ];

          postBuild = ''
            makeWrapperArgs=(${escapeShellArgs cfg.makeWrapperArgs})

            for bin in "$out"/bin/*; do
              link="$(readlink "$bin")"
              rm "$bin"
              makeWrapper "$link" "$bin" "''${makeWrapperArgs[@]}"
            done

            ${cfg.installScript}
          '';
        };
      }
    ];
}
