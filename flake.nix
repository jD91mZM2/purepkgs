{
  description = "A fully configurable builder for self-contained nixpkgs derivations";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: {
    overlay = final: prev: {
      purePkgs = import ./purepkgs {
        pkgs = final;
      };
    };
  } //
  (utils.lib.eachDefaultSystem (system: let
    pkgs = nixpkgs.legacyPackages."${system}".extend self.overlay;
    lib = nixpkgs.lib;
  in rec {
    purePkgs = pkgs.purePkgs;

    packages = purePkgs.packages;

    apps = lib.mapAttrs (_name: pkg: {
      type = "app";
      program = pkg.main;
    }) packages;
  }));
}
