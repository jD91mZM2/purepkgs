{ pkgs ? import <nixpkgs> { } }:
let
  purepkgs = import ../purepkgs { inherit pkgs; };
in
{
  neovim = purepkgs.callPackage ./neovim.nix { };
  less = purepkgs.callPackage ./less.nix { };
  zsh = purepkgs.callPackage ./zsh.nix { };
}
