{ purepkgs, ... }:

purepkgs.packages.less.configure (
  { pkgs, ... }:
  let
    attr = begin: {
      inherit begin;
      end = [ [ "sgr0" ] ];
    };
  in
  {
    colours = {
      bold = attr [ [ "bold" ] [ "setf" "4" ] ];
      underline = attr [ [ "smul" ] [ "setf" "6" ] ];
      standout = attr [ [ "setf" "2" ] [ "setb" "5" ] ];
    };
  }
)
