{ purepkgs, ... }:

purepkgs.packages.zsh.configure ({ pkgs, lib, ... }: {
  autosuggestions = {
    enable = true;
    highlightStyle = "bg=#000,fg=#FFF";
  };
  syntaxHighlighting.enable = true;
  vi.enable = true;
  prompt = ''
    echo "$?" "$(pwd)> "
  '';
  interactiveShellInit = ''
    echo Welcome to ZSH, man!
  '';

  aliases.a = "echo hi";

  program = {
    extraDrvs = with pkgs; [
      pkgs.hello
    ];
    installScript = ''
      mkdir -p "$out/share"
      echo "Hello" > "$out/share/test"
    '';
  };
})
