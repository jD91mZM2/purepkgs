{ purepkgs, ... }:

purepkgs.packages.neovim.configure ({ pkgs, ... }: {
  customRC = ''
    set number
    set relativenumber
  '';
  packages.nixPackages = with pkgs.vimPlugins; {
    start = [ vim-nix ];
  };
})
