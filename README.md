# purepkgs

Purepkgs is a very simple idea: You should be able to make Nix derivations that
include everything they need to include to function. Your shell should include
the programs you run in it, and your terminal should include your shell.
Purepkgs have *pure* derivations, your `$PATH` contains nothing but what the
Nix derivation says it must contain.

Why is this useful?

1. **Unclutter your configs.** Each program will be neatly separated.
2. **Unified config** across globally-installed **NixOS** and locally-installed
   **home-manager**.
3. **Portable.** Each program can easily be passed as a closure to any Nix
   machine, or `nix-bundle`d away.

## How to use

See all Nix files in the `examples/` directory.

Purepkgs has the following structure:

``` nix
# Inputs
{
  # The nixpkgs instance to use
  pkgs,

  # Extra inputs to use
  args ? {},

  # Global packages to always be installed
  globals ? [ pkgs.coreutils ],
}:

# Outputs
{
  # Create a new purepkgs package given the input module or list of modules
  mkPackage = modules: { /* ... */ };

  # Package set of base purepkgs packages ready to be configured
  packages = { /* ... */ };

  # Return purepkgs instance with updated settings
  override = attrs: { /* ... */ }
}
```
